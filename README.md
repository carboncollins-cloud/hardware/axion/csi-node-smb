# Storage Plugin - Axion SMB CSI Node

[[_TOC_]]

## Description

[CSI](https://github.com/container-storage-interface/spec) storage plugin node configurations using [Democratic CSI](https://github.com/democratic-csi/democratic-csi) smb-client for connecting the the Axion NAS within [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud).
