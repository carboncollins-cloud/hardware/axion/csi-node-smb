# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2022-05-08]

### Changed
- Constrain to services role clients

## [2022-05-02]

### Added 
- Meta stanza to job file to allow linking back to original deployment

### Changed
- moved nomad namespace to nas-axion

## [2022-05-01]

### Added
- Updated all links to point to new repository location
- Moved repository to [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) GitLab Group
