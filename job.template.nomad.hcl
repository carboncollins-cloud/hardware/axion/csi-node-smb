locals {
  csi_mode = "node"
  csi_name = "soc-axion-smb"
  csi_version = "[[ .csiVersion ]]"
}

job "storage-plugin-axion-smb-node" {
  name = "Storage Plugin Node (SoC Axion SMB)"
  type = "system"
  region = "se"
  datacenters = ["soc"]
  namespace = "c3-hardware-axion"

  priority = 79

  group "node" {

    constraint {
      attribute = "${node.class}"
      value = "node"
    }

    consul {}

    service {
      name = "soc-axion-smb-csi-node"
      task = "plugin"
    }

    task "plugin" {
      driver = "docker"

      vault {
        role = "service-axion-csi-node"
      }

      config {
        image = "[[ .democraticImage ]]"

        args = [
          "--csi-mode=${local.csi_mode}",
          "--csi-version=${local.csi_version}",
          "--csi-name=${local.csi_name}",
          "--driver-config-file=${NOMAD_TASK_DIR}/driver-config-file.yaml",
          "--server-socket=unix:///csi/csi.sock",
          "--log-level=debug"
        ]

        privileged = true
        ipc_mode = "host"
        network_mode = "host"
      }

      csi_plugin {
        id = "${local.csi_name}"
        type = "${local.csi_mode}"
        mount_dir = "/csi"
        health_timeout = "30s"
      }

      env {
        CSI_NODE_ID = "${attr.unique.hostname}"
      }

      template {
        data = <<EOH
[[ fileContents "./config/driver-config-file.yaml.tpl" ]]
        EOH

        destination = "${NOMAD_TASK_DIR}/driver-config-file.yaml"
      }

      resources {
        cpu = 100
        memory = 150
      }
    }
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "30s"
    healthy_deadline = "5m"
    progress_deadline = "10m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
